#!/usr/bin/env bash

warn() {
	local varname="${1}"
	shift
	echo "Variable '${varname}' has to/should be set correctly!" >&2
	echo $@
}

die() {
	warn $@
	exit 1
}

set_dfs_default() {
	[ -n "${DFS_DEFAULT}" ] && export PROP_CORE_fs_defaultFS="${DFS_DEFAULT}" \
	|| warn DFS_DEFAULT "The default path prefix used by the Hadoop FS client when none is given.\
 This is the URI: protocol specifier, e.g., 'hdfs://'; hostname and port of the namenode for the cluster (its RPC port for the 'dhfs://' protocol)."
}

# Functions to set the YARN properties, see
# https://hadoop.apache.org/docs/current/hadoop-yarn/hadoop-yarn-common/yarn-default.xml

set_rm_addresses() {
	local rm_address rm_addresses="${1}" rm_address_noproto
	for rm_address in ${rm_addresses//,/ }; do
		case "${rm_address}" in
			http://*)
				rm_address_noproto="${rm_address##*://}"
				export PROP_YARN_yarn_resourcemanager_webapp_address="${rm_address_noproto}"
				[ -z "${PROP_YARN_yarn_resourcemanager_hostname}" ] && export PROP_YARN_yarn_resourcemanager_hostname="${rm_address_noproto%%:*}"
				;;
			https://*)
				rm_address_noproto="${rm_address##*://}"
				export PROP_YARN_yarn_resourcemanager_webapp_https_address="${rm_address_noproto}"
				[ -z "${PROP_YARN_yarn_resourcemanager_hostname}" ] && export PROP_YARN_yarn_resourcemanager_hostname="${rm_address_noproto%%:*}"
				;;
			rpc://*)
				rm_address_noproto="${rm_address##*://}"
				export PROP_YARN_yarn_resourcemanager_address="${rm_address_noproto}"
				[ -z "${PROP_YARN_yarn_resourcemanager_hostname}" ] && export PROP_YARN_yarn_resourcemanager_hostname="${rm_address_noproto%%:*}"
				;;
			schrpc://*)
				rm_address_noproto="${rm_address##*://}"
				export PROP_YARN_yarn_resourcemanager_scheduler_address="${rm_address_noproto}"
				[ -z "${PROP_YARN_yarn_resourcemanager_hostname}" ] && export PROP_YARN_yarn_resourcemanager_hostname="${rm_address_noproto%%:*}"
				;;
			rtrpc://*)
				rm_address_noproto="${rm_address##*://}"
				export PROP_YARN_yarn_resourcemanager_resource___tracker_address="${rm_address_noproto}"
				[ -z "${PROP_YARN_yarn_resourcemanager_hostname}" ] && export PROP_YARN_yarn_resourcemanager_hostname="${rm_address_noproto%%:*}"
				;;
			admrpc://*)
				rm_address_noproto="${rm_address##*://}"
				export PROP_YARN_yarn_resourcemanager_admin_address="${rm_address_noproto}"
				[ -z "${PROP_YARN_yarn_resourcemanager_hostname}" ] && export PROP_YARN_yarn_resourcemanager_hostname="${rm_address_noproto%%:*}"
				;;
			*://*)
				die RESOURCE_MANAGER "Unknown protocol in '${rm_address}' (must be one of http://, https://, rpc://, schrpc://, rtrpc://, or admrpc://), or it can be a plain hostname (without the protocol)."
				;;
			*)
				export PROP_YARN_yarn_resourcemanager_hostname="${rm_address}"
		esac
	done
}

set_nm_addresses() {
	local nm_address nm_addresses="${1}" nm_address_noproto
	for nm_address in ${nm_addresses//,/ }; do
		case "${nm_address}" in
			http://*)
				nm_address_noproto="${nm_address##*://}"
				export PROP_YARN_yarn_nodemanager_webapp_address="${nm_address_noproto}"
				[ -z "${PROP_YARN_yarn_nodemanager_hostname}" ] && export PROP_YARN_yarn_nodemanager_hostname="${nm_address_noproto%%:*}"
				;;
			https://*)
				nm_address_noproto="${nm_address##*://}"
				export PROP_YARN_yarn_nodemanager_webapp_https_address="${nm_address_noproto}"
				[ -z "${PROP_YARN_yarn_nodemanager_hostname}" ] && export PROP_YARN_yarn_nodemanager_hostname="${nm_address_noproto%%:*}"
				;;
			rpc://*)
				nm_address_noproto="${nm_address##*://}"
				export PROP_YARN_yarn_nodemanager_address="${nm_address_noproto}"
				[ -z "${PROP_YARN_yarn_nodemanager_hostname}" ] && export PROP_YARN_yarn_nodemanager_hostname="${nm_address_noproto%%:*}"
				;;
			locipc://*)
				nm_address_noproto="${nm_address##*://}"
				export PROP_YARN_yarn_nodemanager_localizer_address="${nm_address_noproto}"
				[ -z "${PROP_YARN_yarn_nodemanager_hostname}" ] && export PROP_YARN_yarn_nodemanager_hostname="${nm_address_noproto%%:*}"
				;;
			csipc://*)
				nm_address_noproto="${nm_address##*://}"
				export PROP_YARN_yarn_nodemanager_collector___service_address="${nm_address_noproto}"
				[ -z "${PROP_YARN_yarn_nodemanager_hostname}" ] && export PROP_YARN_yarn_nodemanager_hostname="${nm_address_noproto%%:*}"
				;;
			*://*)
				die NODE_MANAGER "Unknown protocol in '${nm_address}' (must be one of http://, https://, rpc://, loipc://, or csipc://), or it can be a plain hostname (without the protocol)."
				;;
			*)
				export PROP_YARN_yarn_nodemanager_hostname="${nm_address}"
		esac
	done
}

set_resource_manager() {
	if [ -n "${RESOURCE_MANAGER}" ]; then
		set_rm_addresses "${RESOURCE_MANAGER}"
	elif [ "${ROLE}" = "nodemanager" ]; then
		die RESOURCE_MANAGER "The node manager has to know a hostname or the protocols, addresses and the base ports where a resource manager HTTP, HTTPS, RPC of the manager, scheduler, tracker, and admin API will listen on."
	else
		warn RESOURCE_MANAGER "The resource manager can set its hostname or the protocols, addresses and the base ports where its HTTP, HTTPS, RPC of the manager, scheduler, tracker, and admin API will listen on."
	fi
}

set_node_manager() {
	if [ -n "${NODE_MANAGER}" ]; then
		set_nm_addresses "${NODE_MANAGER}"
	else
		warn NODE_MANAGER "The node manager can set its hostname or the protocols, addresses and the base ports where its HTTP, HTTPS, RPC of the manager, and IPC of its localizer and collector-service API will listen on."
	fi
}

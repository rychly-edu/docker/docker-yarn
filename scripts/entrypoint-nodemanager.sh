#!/usr/bin/env bash

DIR=$(dirname "${0}")

. "${DIR}/hadoop-entrypoint-helpers.sh"

# from docker-hadoop-base/scripts/application-helpers.sh
. "${DIR}/application-helpers.sh"

set_dfs_default

set_node_manager

set_resource_manager

. "${DIR}/hadoop-set-props.sh"

# from docker-hadoop-base/scripts/application-helpers.sh
wait_for

exec su hadoop -c "exec ${HADOOP_HOME}/bin/yarn nodemanager ${@}"

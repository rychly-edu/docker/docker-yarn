#!/bin/sh

case "${ROLE}" in
	nodemanager)
		exec su hadoop -c '${HADOOP_HOME}/bin/yarn node -list | grep -q "^${HOSTNAME}:" || exit 1'
		;;
	resourcemanager)
		exec su hadoop -c '${HADOOP_HOME}/bin/yarn node -list || exit 1'
		;;
	*)
		echo "Unknow role '${ROLE}', cannot perform the health-check." >&2
		exit 0
esac

# Apache YARN Docker Image

[![pipeline status](https://gitlab.com/rychly-edu/docker/docker-yarn/badges/master/pipeline.svg)](https://gitlab.com/rychly-edu/docker/docker-yarn/commits/master)
[![coverage report](https://gitlab.com/rychly-edu/docker/docker-yarn/badges/master/coverage.svg)](https://gitlab.com/rychly-edu/docker/docker-yarn/commits/master)

The image is based on [rychly-docker/docker-hadoop-base](/rychly-edu/docker/docker-hadoop-base).
The version of the base image can be restricted on build by the `HADOOP_VERSION` build argument.

## Build

### The Latest Version by Docker

~~~sh
docker build --pull -t "registry.gitlab.com/rychly-edu/docker/docker-yarn:latest" .
~~~

### All Versions by the Build Script

~~~sh
./build.sh --build "registry.gitlab.com/rychly-edu/docker/docker-yarn" "latest"
~~~

For the list of versions to build see [docker-tags.txt file](docker-tags.txt).

## Run by Docker-Compose

See [docker-compose.yml](docker-compose.yml).

Use the `ROLE` environment variable or the `entrypoint-<role>.sh` script to start the YARN node in a particular role.
The role has to be one of the following:

*	`resourcemanager` -- for a resourcemanager node
*	`nodemanager` -- fot a nodemanager node
